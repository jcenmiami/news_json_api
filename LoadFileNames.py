import os

print(os.listdir("./historical-news"))
AllFileNames = os.listdir("./historical-news")

print("\n\n")

# List for only the files you want to edit. 
htmlFileList = []

# Go throught the list and find those files that contain the words "news" in the file. 
# When you find one, add it to the list. 
for name in AllFileNames:
    if "-news-" in name:
        htmlFileList.append(name)

# Print final list to verify the data.
htmlFileList.sort()
print(htmlFileList)

x=len(htmlFileList)-1
# Go through the HTML list, open the file and replace <footer> with the URL. 
for fileName in reversed(htmlFileList):
    print (fileName)
    #print (htmlFileList[x])
    

    with open('./historical-news/' + fileName, 'r', encoding="utf-8") as file :
        fileData = file.read()
        file.close()
        #print(fileData)

    # First iteration. File you go to index.html and then next file. 
    if "Headline news from:" in fileData:
        print ("Link already exists")

    else:
        
        if x == len(htmlFileList)-1:
            print("This is the first iteration.")

            dateText = htmlFileList[x-1].replace("headline-news-", "")
            dateText = dateText.replace(".html","")

            textReplace = '<div>Headline news from: <a href="../index.html">Main Page</a> >> <a href="./'
            textReplace = textReplace + htmlFileList[x-1] + '">' + dateText + '</a></div>\n\n<div id="footer"></div>'

            fileData = fileData.replace('<div id="footer"></div>', textReplace)
            #print(fileData)
        elif x == 0:
            print ("This is the last Iteration")

            dateText = htmlFileList[x+1].replace("headline-news-", "")
            dateText = dateText.replace(".html","")

            textReplace = '<div>Headline news from: <a href="./'
            textReplace = textReplace + htmlFileList[x+1] + '">' + dateText + '</a></div>\n\n<div id="footer"></div>'

            fileData = fileData.replace('<div id="footer"></div>', textReplace)
            #print(fileData)
        else:
            print ("Just another interation in the middle.")

            dateTextPrevious = htmlFileList[x+1].replace("headline-news-", "")
            dateTextPrevious = dateTextPrevious.replace(".html","")

            dateTextNext = htmlFileList[x-1].replace("headline-news-", "")
            dateTextNext = dateTextNext.replace(".html","")

            textReplace = '<div>Headline news from: <a href="' + htmlFileList[x+1] +'">' + dateTextPrevious + '</a> >> <a href="./'
            textReplace = textReplace + htmlFileList[x-1] + '">' + dateTextNext + '</a></div>\n\n<div id="footer"></div>'

            fileData = fileData.replace('<div id="footer"></div>', textReplace)
            #print(fileData)

    

    x = x - 1
    
    # Write the information changes to the file. 
    with open('./historical-news/' + fileName,'w', encoding="utf-8") as file:
        file.write(fileData.encode('ascii','xmlcharrefreplace').decode('utf-8','xmlcharrefreplace'))
    file.close()

    #Reference for html to replace.
    #<div id="footer"></div>
    #<div>Headline news from: <a href="../index.html">Main Page</a> >> <a href="./headline-news-17-10-13.html">17-10-13</a></div>