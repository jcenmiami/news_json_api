'''

All this does is get a JSON file online and save it as a local text file. 

'''

import urllib.request, json, shutil, datetime, sys, os


with urllib.request.urlopen("https://newsapi.org/v1/articles?source=espn&sortBy=top&apiKey=0bb287fb60924e4e9d300e415a335558") as url:
    data = json.loads(url.read().decode('utf-8', 'xmlcharrefreplace'))
    #data.encode('utf-8')
    #print(data)

    tempData = json.dumps(data, ensure_ascii=False, indent=4).encode('utf-8')
    print(json.dumps(data, ensure_ascii=False, indent=4).encode('utf-8'))
    #data = tempData.decode("utf-8")
    #print(json.dumps(data, ensure_ascii=False).encode('utf8'))

print("\n\n\n")


with open('ESPN.txt', 'w') as outfile:
    json.dump(data, outfile)

mydate = datetime.datetime.now()  # Get the date.
# print(mydate.strftime("%y-%m-%d")) # Print with Format the date.

#Rename a file and save the JSON data to backup.
shutil.copy('ESPN.txt','./historical-news/ESPN_' + mydate.strftime("%y-%m-%d") + '.txt')


# Start creating the HTML with the data.

#writeHTMLFile = open('./headline-news-' + mydate.strftime("%y-%m-%d") + '.html','w')
writeHTMLFile = open('./index.html','w')

writeHTMLFile.write('<html>\n<head>\n')
writeHTMLFile.write('<meta http-equiv="content-type" content="text/html; charset=utf-8">\n')
writeHTMLFile.write('<meta name="viewport" content="width=device-width, initial-scale=1">\n')
writeHTMLFile.write('<title>ESPN Headline News ' + mydate.strftime("%Y-%m-%d") +'</title>\n')
writeHTMLFile.write('<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">\n')
writeHTMLFile.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>\n')
writeHTMLFile.write('<script src="https://code.jquery.com/jquery-1.12.4.js"></script>\n')
writeHTMLFile.write('<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>\n')
writeHTMLFile.write('<script>\n')
writeHTMLFile.write('$( function() {\n')
writeHTMLFile.write('$("#header").load("header.html");\n')
writeHTMLFile.write('$("#footer").load("footer.html");\n')
writeHTMLFile.write('$( "#ESPN" ).accordion();\n')
writeHTMLFile.write('} );\n')
writeHTMLFile.write('</script>\n')

writeHTMLFile.write('\n')
writeHTMLFile.write('\n')

# HTML body starts here
writeHTMLFile.write('</head>\n<body>\n')

writeHTMLFile.write('<div id="header"></div>\n')

# Add the banner.
# Mobile Ad
writeHTMLFile.write('<script async="async" type="text/javascript" src="//go.mobisla.com/notice.php?p=1439300&interactive=1&pushup=1"></script></div>\n')

# Pop Under
writeHTMLFile.write('<div id="popUnder"><script type="text/javascript" src="//go.onclasrv.com/apu.php?zoneid=1439295"></script></div>\n')

writeHTMLFile.write('<div id="ESPN">\n')

for x in data['articles']:
    try:

        # Text for Title
        title = x['title'].encode('ascii','xmlcharrefreplace').decode('utf-8','xmlcharrefreplace')
        title = title.replace('\xa0', ' ')
        title = title.replace('\xe9', '&eacute;')
        title = title.replace("'","&apos;")
        title = title.replace('"','&quot;')

        # Text for Description
        description = x['description'].encode('ascii','xmlcharrefreplace').decode('utf-8','xmlcharrefreplace')
        description = description.replace('\xa0', ' ')
        description = description.replace('\xe9', '&eacute;')
        description = description.replace("'","&apos;")
        description = description.replace('"','&quot;')

        # Start writing area for title.
        writeHTMLFile.write('<h3>')

        #Small Image
        articleImage = x['urlToImage'].encode('utf-8')
        writeHTMLFile.write('<img src="' + articleImage.decode('utf-8') + '" style="width: 50px;vertical-align:middle;" hspace="10" >')

        # Write Title
        writeHTMLFile.write(title)
        writeHTMLFile.write('</h3>\n')
   
        # Start writing Description area for jQuery.
        writeHTMLFile.write('<div><p>')

        # Link to Article
        articleURL = x['url'].encode('utf-8')
        #print(articleURL.decode('utf-8'))
        writeHTMLFile.write('<a href="' + articleURL.decode('utf-8') + '">')


        # Large Image
        writeHTMLFile.write('<img src="' + articleImage.decode('utf-8') + '" style="width: 200px;" align="left" hspace="10" >')
        # Write Description
        writeHTMLFile.write(description)
        writeHTMLFile.write('</a></p></div>\n')

    except ValueError as e:
        print("JC there was an error. ", e)


    


writeHTMLFile.write('</div>\n<div id="footer"></div>\n</body>\n</html>\n')
# Close the file.
writeHTMLFile.close()

# Make a copy of the file to backup in historical
shutil.copy('index.html','./historical-news/headline-news-' + mydate.strftime("%y-%m-%d") + '.html')

# Edit HTML by adding the next page at the bottom. 
print(os.listdir("./historical-news"))
AllFileNames = os.listdir("./historical-news")

# List for only the files you want to edit. 
htmlFileList = []

# Go throught the list and find those files that contain the words "news" in the file. 
# When you find one, add it to the list. 
for name in AllFileNames:
    if "-news-" in name:
        htmlFileList.append(name)

# Print final list to verify the data.
htmlFileList.sort()
print(htmlFileList)

x=len(htmlFileList)-1

# Open the file and save the data to a variable. READ ONLY
with open('./index.html', 'r') as file :
    fileData = file.read()
file.close()
    #print(fileData)

# Modify the data to then write in to the index.html file.
dateText = htmlFileList[x-1].replace("headline-news-", "")
dateText = dateText.replace(".html","")

textReplace = '<div>Headline news from: <a href="./historical-news/'
textReplace = textReplace + htmlFileList[x-1] + '">' + dateText + '</a></div>\n\n<div id="footer"></div>'

fileData = fileData.replace('<div id="footer"></div>', textReplace)
#print(fileData)


# Write the information changes to the file. 
with open('./index.html','w') as file:
    file.write(fileData)
file.close()


# Let me know you are done.
print('JC I\'m Done')