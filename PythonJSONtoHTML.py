'''

All this does is get a JSON file online and save it as a local text file. 

'''

import urllib.request, json, shutil, datetime


with urllib.request.urlopen("https://newsapi.org/v1/articles?source=espn&sortBy=top&apiKey=0bb287fb60924e4e9d300e415a335558") as url:
    data = json.loads(url.read().decode("utf-8"))
    #data.encode('utf-8')
    #print(data)
    print(json.dumps(data, indent=4))

print("\n\n\n")


with open('ESPN.txt', 'w') as outfile:
    json.dump(data, outfile)

mydate = datetime.datetime.now()  # Get the date.
# print(mydate.strftime("%y-%m-%d")) # Print with Format the date.

#Rename a file and save the JSON data to backup.
shutil.copy('ESPN.txt','./historical-news/ESPN_' + mydate.strftime("%y-%m-%d") + '.txt')


# Start creating the HTML with the data.

#writeHTMLFile = open('./headline-news-' + mydate.strftime("%y-%m-%d") + '.html','w')
writeHTMLFile = open('./index.html','w')

writeHTMLFile.write('<html>\n<head>\n')
writeHTMLFile.write('<meta http-equiv="content-type" content="text/html; charset=utf-8">\n')
writeHTMLFile.write('<meta name="viewport" content="width=device-width, initial-scale=1">\n')
writeHTMLFile.write('<title>ESPN Headline News ' + mydate.strftime("%Y-%m-%d") +'</title>\n')
writeHTMLFile.write('<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">\n')
writeHTMLFile.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>\n')
writeHTMLFile.write('<script src="https://code.jquery.com/jquery-1.12.4.js"></script>\n')
writeHTMLFile.write('<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>\n')
writeHTMLFile.write('<script>\n')
writeHTMLFile.write('$( function() {\n')
writeHTMLFile.write('$("#header").load("header.html");\n')
writeHTMLFile.write('$("#footer").load("footer.html");\n')
writeHTMLFile.write('$( "#ESPN" ).accordion();\n')
writeHTMLFile.write('} );\n')
writeHTMLFile.write('</script>\n')

writeHTMLFile.write('\n')
writeHTMLFile.write('\n')


writeHTMLFile.write('</head>\n<body>\n')

writeHTMLFile.write('<div id="header"></div>\n')

writeHTMLFile.write('<div id="ESPN">\n')

for x in data['articles']:
    # Text for Title
    title = x['title'].encode('utf-8')
    writeHTMLFile.write('<h3>')

    #Small Image
    articleImage = x['urlToImage'].encode('utf-8')
    writeHTMLFile.write('<img src="' + articleImage.decode('utf-8') + '" style="width: 50px;vertical-align:middle;" hspace="10" >')

    # Write Title
    writeHTMLFile.write(title.decode('utf-8'))
    writeHTMLFile.write('</h3>\n')

    # Text for Description
    description = x['description'].encode('utf-8')
    
    writeHTMLFile.write('<div><p>')

    # Link to Article
    articleURL = x['url'].encode('utf-8')
    #print(articleURL.decode('utf-8'))
    writeHTMLFile.write('<a href="' + articleURL.decode('utf-8') + '">')


    # Large Image
    writeHTMLFile.write('<img src="' + articleImage.decode('utf-8') + '" style="width: 200px;" align="left" hspace="10" >')
    # Write Description
    writeHTMLFile.write(description.decode('utf-8'))
    writeHTMLFile.write('</a></p></div>\n')


writeHTMLFile.write('</div>\n<div id="footer"></div>\n</body>\n</html>\n')
# Close the file.
writeHTMLFile.close()
shutil.copy('index.html','./historical-news/headline-news-' + mydate.strftime("%y-%m-%d") + '.html')